import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
      //throw new RuntimeException ("Nothing implemented yet!"); // delete this
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (6, 9);
      g.createRandomSimpleGraph(3,3);;
      System.out.println (g);
      Graph clone = g.cloneGraph("C");
      int[][] array = g.createAdjMatrix();
      //System.out.println(g.first.info);
      System.out.println(clone);
      System.out.println(g.areVerticesDifferent(g.first, clone.first));
      System.out.println(g.areArcsDifferent(g.first.first, clone.first.first));
      //System.out.println(Arrays.deepToString(g.createAdjMatrix()));
      //System.out.println(g.cloneGraph("C"));
      //System.out.println(g.cloneGraph("c"));
      Graph oneVertix = new Graph("O");
      oneVertix.first = new Vertex("v1", null, null);
      System.out.println(oneVertix);
      System.out.println(oneVertix.cloneGraph("OC"));
      Graph empty = new Graph("E");
      //System.out.println(empty);
      //System.out.println(empty.cloneGraph("EC"));
      Graph loneWolf = new Graph("L");
      Vertex v1 = new Vertex("v1", null, null);
      //Vertex v4 = new Vertex("v4", null, null);
      //Arc av1_v4 = new Arc("av1_v4", v4, null);
      //Arc av3_v4 = new Arc("av3_v4", v4, null);
      Vertex v3 = new Vertex("v3", null, null);
      Arc av1_v3 = new Arc("av1_v3", v3, null);
      //Arc av4_v3 = new Arc("av4_v3", v3, null);
      Vertex v2 = new Vertex("v2", v3, null);
      v1.next = v2;
      //Arc av4_v1 = new Arc("av4_v1", v1, av4_v3);
      //v4.first = av4_v1;
      Arc av3_v1 = new Arc("av3_v1", v1, null);
      v3.first = av3_v1;
      v1.first = av1_v3;
      loneWolf.first = v1;
      System.out.println(loneWolf);
      System.out.println(loneWolf.cloneGraph("LC"));
      Graph onlyVertices = new Graph("V");
      Vertex v4 = new Vertex("v4", null, null);
      v3 = new Vertex("v3", v4, null);
      v2 = new Vertex("v2", v3, null);
      onlyVertices.first = new Vertex("v1", v2 ,null);
      //System.out.println(onlyVertices.createVertex("lolz").first);
      System.out.println(onlyVertices);
      //System.out.println(Arrays.deepToString(onlyVertices.createAdjMatrix()));
      System.out.println(onlyVertices.cloneGraph("VC"));
      //g.cloneGraph();
      //System.out.println (g.first);
      //System.out.println (g.first.first.target);
      //System.out.println (g.first.next);

   }


   /**
    * Vertex is the main entity to clone a graph.
    * This code does deep cloning, where Arcs and Vertices have also been cloned, in accordance with Java conventions.
    *
    * @author Desiree Himuškin
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Create a clone to an existing graph.
       *
       * @param newGraphName is name which will be given to the cloned graph.
       * @throws IllegalStateException, when given graph is empty.
       *
       * @return cloned graph.
       */

      public Graph cloneGraph(String newGraphName) {

         int[][] adj = this.createAdjMatrix();
         Graph clone = new Graph(newGraphName);
         HashMap<String, Vertex> vertices = new HashMap<>();
         HashMap<String, Arc> oldArcs = new HashMap<>();
         Vertex currentRoot = this.first;

         if (currentRoot == null) {
            throw new IllegalStateException("The graph is empty!");
         }
         Arc currentConnection = this.first.first;

         for (int i = adj.length; i > 0 ; i--) {
            Vertex vertex = clone.createVertex("v" + i);
            vertices.put("v"+i, vertex);
            oldArcs.put(currentRoot.id, currentConnection);
            if (i > 1) {
               currentRoot = currentRoot.next;
               currentConnection = currentRoot.first;
            }
         }
         Stack<Vertex> arcs = new Stack<>();

         for (int i = adj.length; i > 0; i--) {
            currentRoot = vertices.get("v"+ i);
            currentConnection = oldArcs.get(currentRoot.id);

            while (currentConnection != null) {
               Vertex to = vertices.get(currentConnection.target.id);
               arcs.push(to);
               currentConnection = currentConnection.next;
            }
            while (!arcs.empty()) {
               Vertex to = arcs.pop();
               clone.createArc("av" + i + "_" + to.id, currentRoot, to);
            }
         }
         return clone;
      }

      /**
       * Returns hashcodes of vertices with the same id to visually see, whether they are different or not.
       *
       * @param oldV, original Vertex.
       * @param newV, clone of the original vertex.
       *
       * @return String containing original and cloned vertices hashcode.
       */
      public String areVerticesDifferent(Vertex oldV, Vertex newV) {
         return "Original " + oldV.id + " hashcode: " + oldV.hashCode() + ". Cloned vertix hashcode: " + newV.hashCode();
      }

      /**
       * Returns hashcodes of arcs with the same id to visually see, whether they are different or not.
       *
       * @param oldA, original arc.
       * @param newA, clone of the original arc.
       *
       * @return String containing original and cloned arcs hashcode.
       */
      public String areArcsDifferent(Arc oldA, Arc newA) {
         return "Original " + oldA.id + " hashcode: " + oldA.hashCode() + ". Cloned arc hashcode: " + newA.hashCode();
      }

   }

} 
